**What is a Neural Network?**

To understand how an artificial neuron works, we should know how the biological neuron works.

https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRey5b78TTLQAzBLqbT-kS3jujfuN6tr4l_Cw&usqp=CAU


**Dendrites** 
These receive information or signals from other neurons that get connected to it.

**Cell Body**
Information processing happens in a cell body. These take in all the information coming from the different dendrites and process that information.

**Axon** 
It sends the output signal to another neuron for the flow of information. Here, each of the flanges connects to the dendrite or the hairs on the next one. 


**How Do Neural Network Works?**

Consider a neural network shown below:

http://www.texample.net/media/tikz/examples/PNG/neural-network.png

The network starts with an input layer that receives input in the form of data.
The lines connected to the hidden layers are called weights, and they add up on the hidden layers. Each dot in the hidden layer processes the inputs, and it puts an output into the next hidden layer and lastly, into the output layer.

**Gradient Descent**

A mathematical technique that modifies the parameters of a function to descend from a high value of a function to a low value, by looking at the derivatives of the function with respect to each of its parameters, and seeing which step, via which parameter, is the next best step to minimize the function. Applying gradient descent to the error function helps find weights that achieve lower and lower error values, making the model accurate.

https://miro.medium.com/max/1200/1*iNPHcCxIvcm7RwkRaMTx1g.jpeg


**Backpropogation**

Neural networks are trained using a process called backpropagation—this is an algorithm which traces back from the output of the model, through the different neurons which were involved in generating that output, back to the original weight applied to each neuron. Backpropagation suggests an optimal weight for each neuron which results in the most accurate prediction.

https://th.bing.com/th/id/OIP.7ZDt1BdanYyyhFGXp4KGQgHaFf?w=244&h=181&c=7&o=5&dpr=1.25&pid=1.7


