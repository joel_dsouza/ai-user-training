**Convolutional Neural Network (CNN)**

In neural networks, Convolutional neural network (ConvNets or CNNs) is one of the main categories to do images recognition, images classifications. Objects detections, recognition faces etc., are some of the areas where CNNs are widely used.

https://adeshpande3.github.io/assets/Cover.png


**Working**

1. A convolutional neural network can have tens or hundreds of layers that each learn to detect different features of an image.

2. Filters are applied to each training image at different resolutions, and the output of each convolved image is used as the input to the next layer. 

3. The filters can start as very simple features, such as brightness and edges, and increase in complexity to features that uniquely define the object.

CNNs perform feature identification and classification of images, text, sound, and video.

